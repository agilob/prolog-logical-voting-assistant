% A definition of the first politician
% I wanted this policitian to have many views
% Also I have him views to present him as a EU-fan, socialist
supports(politician1, abortion).
supports(politician1, feminism).
supports(politician1, hightax).
supports(politician1, commonwealth).
supports(politician1, benefits).
supports(politician1, pacifism).
supports(politician1, immigration).
supports(politician1, openedborders).
supports(politician1, diversification).
supports(politician1, europeanunion).
supports(politician1, socialism).
supports(politician1, naturalFood).

% here we have the second policitian, who is a nationalist.
supports(politician2, prolife).
supports(politician2, lowtax).
supports(politician2, militarism).
supports(politician2, closedborders).
supports(politician2, strongarmy).
supports(politician2, nationalism).
supports(politician2, foodresearch).
supports(politician2, gmo).

% another nationalist, liberalist,
supports(politician3, war).
supports(politician3, abortion).
supports(politician3, commonwealth).
supports(politician3, benefits).
supports(politician3, profamily).
supports(politician3, immigration).
supports(politician3, strongarmy).
supports(politician3, nationalism).
supports(politician3, gmo).
supports(politician3, liberalism).

% profamily, liberalist, naturalist
supports(politician4, prolife).
supports(politician4, profamily).
supports(politician4, liberalism).
supports(politician4, poverty).
supports(politician4, pacifism).
supports(politician4, nationalism).
supports(politician4, naturalfood).

% here we have a definition of a random voter
likes(voter, profamily).
likes(voter, naturalfood).
likes(voter, pacifism).
likes(voter, closedborders).
likes(voter, liberalism).
likes(voter, strongarmy).
likes(voter, lowtax).


% below I made definitions of similar views
% It is helpful to check if a politican supports a view by
% checking it's synonims (or similar opinions) 

similar(abortion, feminism).
similar(hightax , commonwealth).
similar(lowtax , liberalism).
similar(war, militarism).
similar(war, strongarmy).
similar(strongarmy, militarism).
similar(immigration, openedborders).
similar(diversification, immigration).
similar(europeanunion, openedborders).
similar(gmo, foodresearch).
similar(liberalism, lowtax).
similar(liberalism, pacifism).


% the following evaluations are defined to provide opposite views and opinions to the system
% it can be helpful to check who an user should *not* vote for

opposite(gmo, naturalfood).
opposite(prolife, abortion).
opposite(hightax, liberalism).
opposite(poverty, benefits).
opposite(nationalism, socialism).
opposite(commonweatlh, liberalism).
opposite(militarism, pacifism).


% rule checks if one opinion is opposite to another
% checking rule is not affected by the order of opinions passed
% to this rule, because I wrote two opposite(X, Y) in both ways,
% OR (;) takes care about it

oppo(P1, P2) :-
      opposite(P1, P2);
      opposite(P2, P1).


% rule checks if one opinion is similar to another
% checking rule doesn't care about the order of passed opinions 
% to this rule.

sim(P1, P2) :-
      similar(P1, P2);
      similar(P2, P1).


% this method compares if two opinions are opposite,
% but if they **were NOT** connected to each other in data-sets above;
% examples I tested:
%
% P1 = strongarmy, P4 = liberalims
%
% ?- similar(strongarmy, X).
%   X = militarism.
%
% ?- oppo(militarism, pacifism).
%   true .
% 
% ?- similar(pacifism, liberalism).
%   false.
% 
%% but...
% ?- sim(pacifism, liberalism).
%   true.
%
%% so this method travels the whole data-sets tree in every possible way to
%% find me this:
% ?- still_oposite(strongarmy, liberalism).
%   true .

still_oposite(P1, P4) :- 
      sim(P1, P2),
      oppo(P2, P3),
      sim(P3, P4).

% supports(politician2, prolife). included in data-sets
% supports(politician2, profamily). - not included in data-sets
%
% ?- does_support(politician2, prolife).
%   true .
%
% ?- does_support(politician2, profamily).
%   true .

does_support(POLITICIAN, P1) :-
      sim(P1, P2);
      supports(POLITICIAN, P2).

    
% rule returns all elements which fit to rule suppport(POLITICIAN, Views). 
% Returns a list of views for a given politician

support_list(POLITICIAN, Views) :- 
      findall(Views,  % buit-in rule
            (
            supports(POLITICIAN, Views) % if supports, add to the list
            ),
      Views). % save list here


% Rule checks if specified politician _declares_ (is declared) to be liberal.
% get policitian, get list of views for the politician,
% save the list in Views, check if the list contains label liberalims

is_liberal(POLITICIAN) :-
      support_list(POLITICIAN, Views),
      member(liberalims, Views). 


% method can check if specified politician declares to be a socialist OR
% liberalist OR nationalist. Three at a time.
% `member` methods have to be in one bracket!
% just to play with Prolog.

is_sln(POLITICIAN) :-
      support_list(POLITICIAN, Views), 
      (
            member(socialism, Views);
            member(liberalism, Views);
            member(nationalism, Views)
      ).

% Rule returns a list of all politicians in our data-sets
% ?- politicians_list(L).
%    L = [politician1, politician2, politician3, politician4].
% Mark ! at the end stops execution of any rule, just returns the first result.

politicians_list(L) :- 
      findall(P, (supports(P, _)), P),
      rem_duplicates(P, L), 
      !. % HA!
      % ! mark results in one result, doesn't
      % wait for user's action in interactive commandline

% Rule removes duplicates from given lists.
% This one is used in politicians_list(L), where an infinite list of policitians
% is returned without cleaning from duplicates.
rem_duplicates([], []). % returns emply element

rem_duplicates([First | Rest], NewRest) :- 
      member(First, Rest),        % if element is a memeber
      rem_duplicates(Rest, NewRest). % dig deeper
      
rem_duplicates([First | Rest], [First | NewRest]) :- 
      not(member(First, Rest)),    % if element is not a member
      rem_duplicates(Rest, NewRest).  % dig deeper too, until we have empy list


% rule used in automatic mode.
% automatically finds all voters views and compares them to all politicians
% from our datasets.
% Copied from support_list(POLITICIAN, Views), so I'll skip comments here

voter_views(Views) :- 
      findall(Views,
      (
            likes(voter, Views)
      ),
      Views).


% method recursively finds all common views with provided politican.

agrees(POLITICIAN, [], 0). % return 0 if empty.

agrees(POLITICIAN, [Head|Tail], Num) :- % split list on Head and Tail
      agrees(POLITICIAN, Tail, Numx), % call itself but without head of the list
            ((supports(POLITICIAN, Head), %check if policitian supporst given view
                Num is Numx + 1);  % increment number of views
            (\+supports(POLITICIAN, Head), % if doesnt support
                Num is Numx)). %don't increment, just call itself


% rule does nearly the same as above, but writes on std:out views which are common
% for voter and politian.

agreesw(POLITICIAN, []).
agreesw(POLITICIAN, [Head|Tail]) :- 
      agreesw(POLITICIAN, Tail),
            ((supports(POLITICIAN, Head),
                write(' '),
                write(Head));
            (\+supports(POLITICIAN, Head))).


% recursive rule will print sentences about common views, number of them
% with provided politician
% rule bolds important information like number of common views, politicians name

agree_list([], Views).
agree_list([Head | Tail], Views) :- 
                 agree_list(Tail, Views),
              write('You have \033[1m'),
                 agrees(Head, Views, CommonViewsNo),
              write(CommonViewsNo),
              write('\033[0m common views with \033[1m'),
              write(Head),
              write(': '),
                 agreesw(Head, Views),
              write('\033[0m; It\'s '),
                 length(Views, VotersViewsNo),
              write(CommonViewsNo / VotersViewsNo),
              write(' of your views.'),
              nl.

% automatic mode, will get a list of views assigned to voter
% and call agree_list
% ?- automatic.
%    You have 4 common views with politician4 liberalism pacifism naturalfood profamily; It's 4/7 of your views.
%    You have 3 common views with politician3 strongarmy liberalism profamily; It's 3/7 of your views.
%    You have 3 common views with politician2 lowtax strongarmy closedborders; It's 3/7 of your views.
%    You have 1 common views with politician1 pacifism; It's 1/7 of your views.
%    true.

automatic :-
      voter_views(Views),
      politicians_list(PoliList),
      agree_list(PoliList, Views),
      !. % will stop execution here.


% will ask a user about their views, read them from std:in and compare with all politicians in our datasets
% ?- reading.
%    |: [lowtax, war, profamily].
%    You have 1 common views with politician4 profamily; It's 1/3 of your views.
%    %    You have 2 common views with politician3 profamily war; It's 2/3 of your views.
%    You have 1 common views with politician2 lowtax; It's 1/3 of your views.
%    You have 0 common views with politician1; It's 0/3 of your views.
%    true.

reading :-
    read(Views),
    politicians_list(PoliList),
    agree_list(PoliList, Views),
    !.
